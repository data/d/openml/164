{
  "data_set_description": {
    "default_target_attribute": "class",
    "description": "**Author**: C. Harley, R. Reynolds, M. Noordewier, J. Shavlik.  \n**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/Molecular+Biology+(Promoter+Gene+Sequences)) - 1990  \n**Please cite**: [UCI](https://archive.ics.uci.edu/ml/citation_policy.html)  \n\n**E. coli promoter gene sequences (DNA)**  \nCompilation of promoters with known transcriptional start points for E. coli genes. The task is to recognize promoters in strings that represent nucleotides (one of A, G, T, or C). A promoter is a genetic region which initiates the first step in the expression of an adjacent gene (transcription).  \n\nThe input features are 57 sequential DNA nucleotides. Fifty-three sample promoters and 53 nonpromoter sequences were used. The 53 sample promoters were obtained from a compilation\nproduced by Hawley and McClure (1983). Negative training examples were thus derived by selecting contiguous substrings from a 1.5 kilobase sequence provided by Prof. T. Record of the Univ. of Wisconsin’s Chemistry Dept. This sequence is a fragment from E. coli bacteriophage T7 isolated with the restriction enzyme HaeIII. By virtue of the fact that the fragment does not bind RNA polymerase, it is believed to not contain any promoter sites.\n\nThis dataset has been developed to help evaluate a \"hybrid\" learning algorithm (\"KBANN\") that uses examples to inductively refine preexisting knowledge.\n\n### Attribute Description  \n\n* 1. One of {+/-}, indicating the class (\"+\" = promoter).\n* 2. The instance name (non-promoters named by position in the 1500-long nucleotide sequence provided by T. Record).\n* 3-59. The remaining 57 fields are the sequence, starting at position -50 (p-50) and ending at position +7 (p7). Each of these fields is filled by one of {a, g, t, c}.\n \n### Relevant papers  \n\n* Harley, C. and Reynolds, R. 1987. \"Analysis of E. Coli Promoter Sequences.\" Nucleic Acids Research, 15:2343-2361.  \n* Towell, G., Shavlik, J. and Noordewier, M. 1990. \"Refinement of Approximate Domain Theories by Knowledge-Based Artificial Neural Networks.\" In Proceedings of the Eighth National Conference on Artificial Intelligence (AAAI-90).",
    "description_version": "1",
    "file_id": "3585",
    "format": "ARFF",
    "id": "164",
    "licence": "Public",
    "md5_checksum": "51cdf7757673b47cadc65d6fbd223fd7",
    "minio_url": "https://data.openml.org/datasets/0000/0164/dataset_164.pq",
    "name": "molecular-biology_promoters",
    "parquet_url": "https://data.openml.org/datasets/0000/0164/dataset_164.pq",
    "processing_date": "2020-11-20 19:45:17",
    "row_id_attribute": "instance",
    "status": "active",
    "tag": [
      "Bioinformatics",
      "Biology",
      "Computational Biology",
      "Genetics",
      "study_1",
      "study_123",
      "study_50",
      "study_52",
      "study_7",
      "study_88",
      "uci"
    ],
    "upload_date": "2014-04-23T13:11:40",
    "url": "https://api.openml.org/data/v1/download/3585/molecular-biology_promoters.arff",
    "version": "1",
    "version_label": "1",
    "visibility": "public"
  }
}