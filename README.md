# OpenML dataset: molecular-biology_promoters

https://www.openml.org/d/164

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: C. Harley, R. Reynolds, M. Noordewier, J. Shavlik.  
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/Molecular+Biology+(Promoter+Gene+Sequences)) - 1990  
**Please cite**: [UCI](https://archive.ics.uci.edu/ml/citation_policy.html)  

**E. coli promoter gene sequences (DNA)**  
Compilation of promoters with known transcriptional start points for E. coli genes. The task is to recognize promoters in strings that represent nucleotides (one of A, G, T, or C). A promoter is a genetic region which initiates the first step in the expression of an adjacent gene (transcription).  

The input features are 57 sequential DNA nucleotides. Fifty-three sample promoters and 53 nonpromoter sequences were used. The 53 sample promoters were obtained from a compilation
produced by Hawley and McClure (1983). Negative training examples were thus derived by selecting contiguous substrings from a 1.5 kilobase sequence provided by Prof. T. Record of the Univ. of Wisconsin’s Chemistry Dept. This sequence is a fragment from E. coli bacteriophage T7 isolated with the restriction enzyme HaeIII. By virtue of the fact that the fragment does not bind RNA polymerase, it is believed to not contain any promoter sites.

This dataset has been developed to help evaluate a "hybrid" learning algorithm ("KBANN") that uses examples to inductively refine preexisting knowledge.

### Attribute Description  

* 1. One of {+/-}, indicating the class ("+" = promoter).
* 2. The instance name (non-promoters named by position in the 1500-long nucleotide sequence provided by T. Record).
* 3-59. The remaining 57 fields are the sequence, starting at position -50 (p-50) and ending at position +7 (p7). Each of these fields is filled by one of {a, g, t, c}.
 
### Relevant papers  

* Harley, C. and Reynolds, R. 1987. "Analysis of E. Coli Promoter Sequences." Nucleic Acids Research, 15:2343-2361.  
* Towell, G., Shavlik, J. and Noordewier, M. 1990. "Refinement of Approximate Domain Theories by Knowledge-Based Artificial Neural Networks." In Proceedings of the Eighth National Conference on Artificial Intelligence (AAAI-90).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/164) of an [OpenML dataset](https://www.openml.org/d/164). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/164/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/164/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/164/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

